#ifndef IMAGE_CHANGE_H
#define IMAGE_CHANGE_H

#include <stdio.h>
#include <stdint.h>

struct image image_turn(struct image const source);
struct image image_create(const uint64_t w, const uint64_t h);
void image_destroy(struct image *img);

#endif