#ifndef IMAGE_H
#define IMAGE_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct pixel { uint8_t r, g, b; };

enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS = 1,
    READ_INVALID_HEADER = 2,
    FILE_NOT_FOUND = 3,
    READ_INVALID_SIGNATURE = 4,
};
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 1
};

enum read_status bmp_read(FILE* in, struct image* img);
enum write_status bmp_write(FILE* file, struct image img);
#endif