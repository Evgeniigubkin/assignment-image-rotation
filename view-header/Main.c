#include "Image.h"
#include "Image_change.h"
#include "Operations.h"
#define buf_size 1000

int main(){
    const char* filename;
    struct image image0={0};
    struct image image1={0};
    char str[1001];
    enum write_status ws=WRITE_OK;

    /* open file and checking an open file */
    const char* promp = "Write the input name of the source file, for example: C:\\Ci\\1.bmp ";
    filename = read_filename(str, promp,buf_size); // чтение имени будущего файла
    FILE* file= fopen(filename,"rb"); // открытие файла
    checking_file1(file, filename); // проверка файла,checkingFile

    /* reading the image */
    enum read_status rs= bmp_read(file, &image0);//считываем изображение
    /*checking for errors*/
    if (rs==READ_INVALID_HEADER){fclose(file);
    fprintf(stderr,"%s", error_message(rs,ws));exit(rs);}
    if (rs==READ_INVALID_BITS){image_destroy(&image0);
    fprintf(stderr,"%s", error_message(rs,ws));exit(rs);}
    fclose(file);

    /* image changes */
    promp = "Specify the directory where the image will be saved:C:\\Ci\\2.bmp";
    filename = read_filename(str, promp,buf_size); // считываем имя куда поместить
    fopen(filename,"wb"); // создаем файл
    image1 = image_create(image0.width, image0.height); //выделяем память
    image1= image_turn(image0);// поворот картинки
    image_destroy(&image0); // удаляем старое изображение

    /* saving a new image */
    ws = bmp_write(file, image1); // запись
    if (ws==WRITE_OK){
        image_destroy(&image1);
        fclose(file);
        printf("%s",error_message(rs,ws));
     return 0;
     } else {image_destroy(&image1);
        fprintf(stderr,"%s", error_message(rs,ws));
        exit(ws);}//program termination with an error
    }