#include "Image.h"
#include "Image_change.h"
#define BM 0x4D42
#define BMP_HEADER( BMP ) \
        BMP( uint16_t,bfType)\
        BMP( uint32_t,bfileSize)\
        BMP( uint32_t,bfReserved)\
        BMP( uint32_t,bOffBits)\
        BMP( uint32_t,biSize)\
        BMP( uint32_t,biWidth)\
        BMP( uint32_t,biHeight)\
        BMP( uint16_t,biPlanes)\
        BMP( uint16_t,biBitCount)\
        BMP( uint32_t,biCompression)\
        BMP( uint32_t,biSizeImage)\
        BMP( uint32_t,biXPelsPerMeter)\
        BMP( uint32_t,biYPelsPerMeter)\
        BMP( uint32_t,biClrUsed)\
        BMP( uint32_t,biClrImportant)
#define DECLARING( a, b ) a b ;

uint32_t find_wxy(const uint64_t width, const uint64_t x, const uint64_t y);
static uint32_t padding(const uint64_t mx){return (3*mx) % (4);}

struct __attribute__((packed)) bmp_header{BMP_HEADER( DECLARING )};

struct bmp_header header_bmp_create(const uint64_t w, const uint64_t h){
    struct bmp_header bmpHeader = {0};
    bmpHeader.bfType= BM;
    bmpHeader.bfileSize= 54 + padding(w) * h;
    bmpHeader.bfReserved=0;
    bmpHeader.bOffBits=54;
    bmpHeader.biSize=40;
    bmpHeader.biWidth=h;
    bmpHeader.biHeight=w;
    bmpHeader.biPlanes=1;
    bmpHeader.biBitCount=24;
    bmpHeader.biCompression=0;
    bmpHeader.biSizeImage= bmpHeader.bfileSize - bmpHeader.bOffBits;
    bmpHeader.biXPelsPerMeter=4724;
    bmpHeader.biYPelsPerMeter=4724;
    bmpHeader.biClrUsed=0;
    bmpHeader.biClrImportant=0;
    return bmpHeader;
}

static enum read_status header_read(FILE* in, struct bmp_header* bmpHeader1) {
    const size_t str=fread(bmpHeader1, sizeof(struct bmp_header), 1, in);
    if ((str!=1) || (bmpHeader1->bfType != BM) || (bmpHeader1->biBitCount != 24)) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

enum read_status bmp_read(FILE* in, struct image* img) {
    struct bmp_header* bmpHeader1 = {0};
    bmpHeader1 = malloc(sizeof bmpHeader1);
    enum read_status rs= header_read(in, bmpHeader1);
    if(rs!=READ_OK){return rs;}
    *img = image_create(bmpHeader1->biWidth,bmpHeader1->biHeight);
    const uint32_t mx = bmpHeader1->biWidth;
    const uint32_t my = bmpHeader1->biHeight;
    for(size_t i = 0; i < my; ++i){
        const size_t n= find_wxy(mx, 0, i);
        size_t str=fread(&img->data[n], sizeof(struct pixel), mx, in);
        if (str!=mx){
            free(img->data);
            return READ_INVALID_BITS;
        }
        const uint32_t ost=padding(mx);
        if(0<ost){
            uint8_t tmp=0;
            if (fread(&tmp, ost, 1, in)!=1){
                return READ_INVALID_BITS;
            }
        }
    }
    return READ_OK;
}

static enum write_status header_write(FILE* file,const struct image* img){
    struct bmp_header bmpHeader = header_bmp_create(img->width, img->height);
    const size_t st = fwrite(&bmpHeader, sizeof(bmpHeader), 1, file);
    if (st==1){
        return WRITE_OK;
    }
    return WRITE_ERROR;
}

enum write_status bmp_write(FILE* file, struct image img){
    const uint32_t mx = img .width;
    const uint32_t my = img.height;
    enum write_status ws = header_write(file, &img);
    if (ws!=WRITE_OK){return ws;}
    for (size_t i = 0; i < my; i++) {
        const size_t n = find_wxy(mx, 0, i);
        const size_t fw= fwrite(&img.data[n], sizeof(struct pixel), mx, file);
        if (fw != mx) {
            return WRITE_ERROR;
        }
        const uint32_t ost = padding(mx);
        for (uint32_t j = 0; j < ost; j++) {
            uint8_t tmp = 0;
            if (fwrite(&tmp, 1, sizeof(uint8_t), file) == 0) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}