#include "Operations.h"

void checking_file1(FILE* file, const char* filename){
    if(file == NULL) {
        fprintf(stderr,"File <%s> not found\n", filename);
        fprintf(stderr,"The program is completed(-_-)");
        exit(FILE_NOT_FOUND);
    }
}

 const char* error_message(enum read_status readStatus,enum write_status writeStatus){
     if (readStatus==READ_INVALID_HEADER || readStatus==READ_INVALID_BITS || writeStatus == WRITE_ERROR)
         return "Error reading or writing to file.The program is completed(-_-)";
     else return "The file was created successfully";
}