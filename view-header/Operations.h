#ifndef OPERATIONS_H
#define OPERATIONS_H

#include <stdio.h>
#include "Image.h"

const char* error_message(enum read_status readStatus,enum write_status writeStatus);
const char* read_filename(char str[], const char* promp,size_t buf_size);
void checking_file1(FILE* file, const char* filename);
#endif
