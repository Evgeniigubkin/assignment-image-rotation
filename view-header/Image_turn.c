#include "Image.h"
#include "Image_change.h"

uint32_t find_wxy(const uint64_t width, const uint64_t x, const uint64_t y){
    return width*y+x;
}

struct image image_turn(struct image const source) {
    const uint32_t my = source.width;
    const uint32_t mx = source.height;
    struct image dest=image_create(source.width, source.height);
    for (uint32_t x = 0; x < mx ; x++) {
        for (uint32_t y = 0; y < my; y++) {
            dest.data[find_wxy(my, my - y - 1, x)]=source.data[find_wxy(mx, x, y)];
        }
    }
    return dest;
}