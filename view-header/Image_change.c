#include "Image.h"
#include "Image_change.h"

struct image image_create(const uint64_t w, const uint64_t h){
    struct image img ={0};
    img.width = w;
    img.height = h;
    img.data = malloc(w * h * sizeof(struct pixel));
    return img;
}

void image_destroy(struct image *image){
    if(image->data!=NULL){
        free(image->data);
    }
    image->data=NULL;
}
