#include <stdio.h>
#include <string.h>
#include "Operations.h"


const char* read_filename(char* str, const char* promp,size_t buf_size){
    printf("%s", promp);
    printf("\n");
    fgets( str, buf_size, stdin );
    size_t i = strlen(str)-1;
    if(str[i]=='\n') str[i] = '\0';
    return str;
}